import { setSingletonBaseUrl } from 'javier-sedano-ufe5-lib-login';

export default defineNuxtPlugin(nuxtApp => {
    const config = useRuntimeConfig();

    setSingletonBaseUrl(config.public.baseUrl);
});
